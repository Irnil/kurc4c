//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ValEdit.hpp>
#include <ComCtrls.hpp>
#include <MPlayer.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TStaticText *StaticText1;
        TTimer *Timer1;
        TButton *btnAdd;
        TButton *btnEdit;
        TButton *btnDel;
        TButton *btnSave;
        TListBox *lstDate;
        TButton *btnInfo;
        TDateTimePicker *dtpDate;
        TButton *btnSwitch;
        TMediaPlayer *MediaPlayer1;
        TOpenDialog *OpenDialog1;
        TButton *btnLoadSound;
        TComboBox *cmbSound;
        TLabel *Label1;
        TListBox *lstSound;
        TSaveDialog *SaveDialog1;
        void __fastcall Timer1Timer(TObject *Sender);
        void __fastcall btnAddClick(TObject *Sender);
        void __fastcall lstDateClick(TObject *Sender);
        void __fastcall btnDelClick(TObject *Sender);
        void __fastcall btnInfoClick(TObject *Sender);
        void __fastcall btnSwitchClick(TObject *Sender);
        void __fastcall btnLoadSoundClick(TObject *Sender);
        void __fastcall btnSaveClick(TObject *Sender);
        void __fastcall lstSoundClick(TObject *Sender);
        void __fastcall btnEditClick(TObject *Sender);
    

 
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
