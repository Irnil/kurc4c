//---------------------------------------------------------------------------

#include <vcl.h>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sstream>
#include <typeinfo>

#include "Unit1.h"
using namespace std;
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

bool timeDate = False;
bool pastAlarm = False;
TStringList* buf = new TStringList();
String FileName = ExtractFilePath(Application->ExeName);
AnsiString homeDir = GetCurrentDir();
AnsiString mediaDir = homeDir + "/media/";
TSearchRec sr;

TForm1 *Form1;

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{

      int FileHandle;

      homeDir = GetCurrentDir();
        dtpDate->DateTime = Now();
        Label1->Caption = FileName;

         SetCurrentDir(homeDir);

        if (FileExists("SoundList.txt")) {
          cmbSound->Items->LoadFromFile("SoundList.txt");
        }
        else {
          ShowMessage("���� SoundList.txt �� ������! ������ ������ �����.");
          FileHandle = FileCreate("SoundList.txt");
          FileClose(FileHandle);
        }

        if (FileExists("AlarmList.txt")) {
          lstDate->Items->LoadFromFile("AlarmList.txt");
        }
        else {
          ShowMessage("���� AlarmList.txt �� ������! ������ ������ �����.");
          FileHandle = FileCreate("AlarmList.txt");
          FileClose(FileHandle);
        }

        if (FileExists("AlarmSound.txt")) {
          lstSound->Items->LoadFromFile("AlarmSound.txt");
        }
        else {
          ShowMessage("���� AlarmSound.txt �� ������! ������ ������ �����.");
          FileHandle = FileCreate("AlarmSound.txt");
          FileClose(FileHandle);
        }

        for(int i = 0; i < lstDate->Count; i++){
            if(lstDate->Items->Strings[i] < Now()){
              lstDate->Items->Delete(i);
              lstSound->Items->Delete(i);
              pastAlarm = True;
            }
        }
        if(pastAlarm == True){
             pastAlarm = False;
             if(lstDate->Count <= 0){
               lstSound->Items->Add("");
               lstDate->Items->Add("");
               lstSound->ItemIndex = 0;
               lstDate->ItemIndex = 0;
             if(lstSound->Count <= 1){
               cmbSound->Text = "";
             }
                else{
                        lstSound->Items->Strings[0];
                }
                }
             else if(lstDate->Items->Strings[0] == ""){
                 dtpDate->DateTime = Now();
                }
             ShowMessage("���������� � ������� �������!");
        }

        cmbSound->Clear();
        if (FindFirst(".\\media\\*.wav*", faAnyFile | faDirectory, sr) == 0){
                cmbSound->Items->Add(sr.Name);
                while (FindNext(sr) == 0)
               cmbSound->Items->Add(sr.Name);
        }
        FindClose(sr);

        lstSound->ItemIndex = 0;
        lstDate->ItemIndex = 0;
        cmbSound->Text = lstSound->Items->Strings[0];

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Timer1Timer(TObject *Sender)
{       int lstDateLen;


        //lblSound->Caption = typeid(lstDate->ItemIndex).name();

        StaticText1->Caption = Now().TimeString();
        Form1->Caption = "����-���������. �������: " + Now().DateString();

        if(lstSound->Count <=0 || lstDate->Count <= 0){
          lstSound->Items->Add("");
          lstDate->Items->Add("");

        }

        if(lstSound->Focused()){
            lstDate->ItemIndex = lstSound->ItemIndex;
           // dtpDate->DateTime = lstDate->Items->Strings[lstDate->ItemIndex];
            if(lstSound->Count <= 1){
              dtpDate->DateTime = Now();
            }
            else{
              dtpDate->DateTime = lstDate->Items->Strings[lstDate->ItemIndex];
            }
        }
        else if(lstDate->Focused()){
            lstSound->ItemIndex = lstDate->ItemIndex;

            if(lstDate->Count <= 1){
              dtpDate->DateTime = Now();
            }
            else{
              dtpDate->DateTime = lstDate->Items->Strings[lstDate->ItemIndex];
              cmbSound->Text = lstSound->Items->Strings[lstSound->ItemIndex];
            }  
        }
        else if(lstSound->Count <= 0){
            lstSound->ItemIndex = 0;
            lstDate->ItemIndex = 0;
            if(lstSound->Count > 0){
              cmbSound->Text = lstSound->Items->Strings[lstSound->ItemIndex];
            }
            else{
              cmbSound->Text = "";
            }

        }

          for(int i = 0; i < lstDate->Count; i++){
            if(lstDate->Items->Strings[i] == Now()){
              SetCurrentDir(mediaDir);
              MediaPlayer1->FileName = mediaDir + lstSound->Items->Strings[i];

              MediaPlayer1->Open();
              MediaPlayer1->Play();
              ShowMessage("��������!");
              MediaPlayer1->Stop();
              MediaPlayer1->Close();
              //lstDate->Items->Delete(i);
              //lstSound->Items->Delete(i);
            }
          }



}
//---------------------------------------------------------------------------

void __fastcall TForm1::btnAddClick(TObject *Sender)
{

        if(lstDate->Items->Strings[0] == ""){
           lstDate->Items->Delete(0);
           lstSound->Items->Delete(0);
        }

        if(dtpDate->DateTime < Now()){
             ShowMessage("������ ������� ���������� � �������! \n ���������� � ������� �������!");
        }
        else if(cmbSound->ItemIndex < 0){
             ShowMessage("����� ������� �������");
           }
        else{
          lstDate->Items->Add(dtpDate->Time);
          lstSound->Items->Add(cmbSound->Text);
        }

}
//---------------------------------------------------------------------------
void __fastcall TForm1::lstDateClick(TObject *Sender)
{
   if(lstDate->Count <= 0){
     lstSound->Items->Add("");
     lstDate->Items->Add("");
     lstSound->ItemIndex = 0;
     lstDate->ItemIndex = 0;
     if(lstSound->Count <= 1){
       cmbSound->Text = "";
     }
     else{
       lstSound->Items->Strings[0];
     }
   }
   else if(lstDate->Items->Strings[lstDate->ItemIndex] == ""){
     dtpDate->DateTime = Now();
   }
   else{
     dtpDate->DateTime = lstDate->Items->Strings[lstDate->ItemIndex];
   }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::btnDelClick(TObject *Sender)
{
   int index = lstDate->ItemIndex;

   lstDate->Items->Delete(index);
   lstSound->Items->Delete(index);

   lstSound->ItemIndex = 0;
   lstDate->ItemIndex = 0;
   if(lstSound->Count <= 1){
     cmbSound->Text = "";
   }
   else{
     lstSound->Items->Strings[0];
   }


}
//---------------------------------------------------------------------------
void __fastcall TForm1::btnInfoClick(TObject *Sender)
{
   ShowMessage("��������� ����-��������� \n\n 1) ��� ����������� ������ ���������� �������� � ����� �����, ����. ��� ������������ ��������� ������ <�����/����>, ����� ������� �������� \n\n 2) ��� �������������� �������� �������� ������ ��� ��������� � ������, \n ����� ���� ��� ��������� �������� � ����� ��������������. ������� ��������� � ������� ������������� \n\n 3) ��� �������� ��������� ���������� �������� ��������� � ������ � ������� ������� \n\n 4) ��� ���������� ��������� ������� ��������� ����� ������� �� ���������. \n\n �����: ����� ���������� �� ����� ���� � �������. ���� ��� ���, �� ��������� ��������! \n ����� ���������� ������� ��������� ��������, �.�. ������������ ������� ��� ������ �������! \n \n ������� ��������� 4 ����� ������� �������, ������ �-41");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::btnSwitchClick(TObject *Sender)
{

  if(timeDate == False){
    dtpDate->Kind = dtkDate;
    timeDate = True;
  }
  else {
   dtpDate->Kind = dtkTime;
   timeDate = False;
  }
  
}
//---------------------------------------------------------------------------

void __fastcall TForm1::btnLoadSoundClick(TObject *Sender)
{
        AnsiString bufferOut, mediaPath, mediaFile;

        bool clear = False;

        OpenDialog1->Execute();
        bufferOut = OpenDialog1->FileName;
         
        mediaFile = ExtractFileName(OpenDialog1->FileName);
        SetCurrentDir(mediaDir);

        CopyFile((bufferOut).c_str(), (mediaDir+mediaFile).c_str(), true);

        clear = True;
        SetCurrentDir(homeDir);


        if(clear == True){
        cmbSound->Clear();
        if (FindFirst(".\\media\\*.wav*", faAnyFile | faDirectory, sr) == 0){
                cmbSound->Items->Add(sr.Name);
                while (FindNext(sr) == 0)
               cmbSound->Items->Add(sr.Name);
        }
        FindClose(sr);
        clear == False;
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::btnSaveClick(TObject *Sender)
{

  SetCurrentDir(homeDir);

   /*for(int i = 0; i < lstDate->Count; i++){
            if(lstDate->Items->Strings[i] < Now()){
              lstDate->Items->Delete(i);
              lstSound->Items->Delete(i);
              pastAlarm = True;
            }
        }
        if(pastAlarm == True){
             pastAlarm = False;
             ShowMessage("���������� � ������� �������!");
        }  */

  cmbSound->Items->SaveToFile("SoundList.txt");
  lstDate->Items->SaveToFile("AlarmList.txt");
  lstSound->Items->SaveToFile("AlarmSound.txt");

}
//---------------------------------------------------------------------------


void __fastcall TForm1::lstSoundClick(TObject *Sender)
{

   cmbSound->Text = lstSound->Items->Strings[lstSound->ItemIndex];
          
         if(lstDate->Selected[lstDate->ItemIndex] == true){
           lstSound->Selected[lstSound->ItemIndex] == true;
         }

}



//---------------------------------------------------------------------------

void __fastcall TForm1::btnEditClick(TObject *Sender)
{
  //
     if(dtpDate->DateTime < Now()){
             ShowMessage("������ ������� ���������� � �������! \n ���������� � ������� �������!");
        }
        else if(cmbSound->ItemIndex < 0){
             ShowMessage("����� ������� �������");
           }
        else{
           lstSound->Items->Strings[lstSound->ItemIndex] = cmbSound->Text ;
           lstDate->Items->Strings[lstSound->ItemIndex] = dtpDate->DateTime;
        }
}
//---------------------------------------------------------------------------

